from .dev import *

INSTALLED_APPS += [  # noqa: F405
    # Aldryn News&Blog
    'aldryn_apphooks_config',
    'aldryn_common',
    'aldryn_categories',
    'aldryn_newsblog',
    'aldryn_people',
    'aldryn_translation_tools',
    'parler',
    'sortedm2m',
    'taggit',
]
