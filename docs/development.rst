Development
###########

Please mind ``Makefile`` which can help you a lot with development. You should start
with::

    make prepare-dev

Which will install all you need to run this project, test and lint the code and also build
documentation or application. You can read all other commands by just typing ``make``. For
example you can use those commands::

    make test
    make lint
    make run-example

And so on. Very good one is ``run-example`` which is ready for you tu run example project
without anything special so you can test this library. Keep in mind that ``run-example``
is intended for development only, it runs Django's development :djadmin:`runserver`.

Note that every command will check if you made changes to requirements and will download
and install new libraries (or just versions) when needed. You don't need to care about
venv if you will use Makefile.

Documentation
*************

Please, make sure to document everything. The best thing is to have documentation next
to code so it's easy to update and hard (at least harder) to forget about it.

Use ``__init__`` file of every module to describe that module. Also describe all classes
and also methods or functions. Don't forget to include them in documentation.

Tests
*****

Everything should have also it's tests. We use pytest. Every plugin or app has it's own
test module called ``tests`` but all of them share same test utils at the top of the repository.
It's the place where you should store everything you need to easily test features for
Django or Django CMS or anything else, including shared pytest fixtures.

.. _templates:

Templates
*********

It's very hard to do app which is generic and can provide base templates so it's not needed
to change everything. Project `Aldryn Boilerplates <https://github.com/aldryn/aldryn-boilerplates>`_
already took care of it: you just need to install their app and then every Django app have
to have boilerplate directory with all supported boilerplates, for example Bootstrap, and main
app only say what bailerplate want to use.

So it can be easily switchable, but, of course, you have to write for every boilerplate new templates
for every app. Right now we support only Bootstrap3. It's ready in a lot of apps out there
and we like it. It looks like it's useless to use it, which is maybe true, but one day when we will
need to support new style (for example Bootstrap4), we will "just" add all templates, the old sites will
still work and new one will have simple way to use different base templates.

Dynamic templates
=================

Base templates has to be configured by ``CMS_TEMPLATES`` which means it has to be prepared by
developer. Django CMS QE contains only one base empty template and every project needs to provide
their own. At least for now. If you want to keep blank template and just add yours, add something
like this to your settings:

.. code-block:: python

    CMS_TEMPLATES += [
        ('your_page/home.html', 'Your page base template'),
    ]

Of course user can use Bootstrap plugin to create for himself page as he wants but that structure
has to be at every page. It can be automated by Page Types but anyway it's not comfy for user.

Examples
********

Minimal Django app built on top of Django CMS QE is in directory ``example`` and you can start it
simply by::

    make run-example
