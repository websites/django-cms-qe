External plugins
################

Django CMS QE is shipped with following extrnal plugins and apps.

Images and files
****************

`Project page <https://github.com/divio/cmsplugin-filer>`__

Plugin which provides inserting images, galleries and folder views into page.

You can use it simply by adding new plugin from section **Filer** and then choose what folder or
image(s) to show. Note that **Folder** plugin have several options to display in **Style** drop-down list:

:List: list of all files in folder
:SlideShow: slideshow of images in selected folder
:Gallery: gallery of images in adaptive grid format with modal windows to show descriptions and zoom images

Forms
*****

`Project page <https://github.com/aldryn/aldryn-forms>`__

Plugin and app which can help you to create any form you want. Collected data are
stored in database and can be also emailed to configured address after every submission
if needed.

You can use it simply by adding new plugin called **Form** from section **Forms** and then add any
sub-plugins with form fields from section **Form Fields**.

CKEditor
********

`Project page <https://github.com/divio/djangocms-text-ckeditor>`__

Plugin to provide wysiwyg text editor for the users.

Note difference between CKEditor for admin users and for regular users. CKEditor is included
in Django CMS and is used for editing texts on the page. Its config is in `CKEDITOR_CONFIGS`.
This plugin is a plugin to show editor for the regular user and you can configure it in
`CKEDITOR_SETTINGS`.

Bootstrap 4
***********

`Project page <https://github.com/django-cms/djangocms-bootstrap4>`__

Plugins of some Bootstrap components.

For the information about Bootstrap follow `Bootstrap documentation <https://getbootstrap.com/>`_.

Google Map
**********

`Project page <https://github.com/divio/djangocms-googlemap>`__

Plugin which provides inserting Google map with all possible configuration what Google supports
with and any routes or markers.

You can use it simply by adding new plugin called **Google Map** from section **Generic** and then
add any marker or route by adding sub-plugins if needed.
