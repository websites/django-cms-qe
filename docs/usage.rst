Usage
#####

Installation
************

First install Django CMS QE and all needed packages. Just install it
from PyPI (or similarly with ``setup.py``):

.. code-block:: bash

    pip install django-cms-qe

And then install all needed C dependencies. For example for Debian-like
systems:

.. code-block:: bash

    apt-get install \
	    libffi-dev # Library for passwords hashing (is using for Argon2) \
	    zlib1g-dev libjpeg-dev libfreetype6-dev python-dev # Install graphic libraries for captcha

If you are developing library, you can use shortcut with our ``Makefile``:

.. code-block:: bash

    make prepare-dev

Creating new Django app
***********************

There is nothing special. Just create your new web app as you want.
The minimal working example you can find in directory ``example``.
You just need to include base URLs, minimal ``wsgi.py`` script generated
by Django and include base settings where you should provide your specific
settings like ``ROOT_URLCONF``, ``WSGI_APPLICATION``, ``STATIC_ROOT``,
``MEDIA_ROOT`` or ``DATABASES``. For working outside dev environment
also don't forget to set ``SECURE_KEY``, ``ALLOWED_HOSTS`` and other
settings needed for production use.

Example of minimal configuration:

.. code-block:: python

    INSTALLED_APPS += [
        'example',
    ]

    ROOT_URLCONF = 'example.urls'
    WSGI_APPLICATION = 'example.wsgi.application'

    BASE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..')
    STATIC_ROOT = os.path.join(BASE_DIR, 'static')
    MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, '..', 'db.sqlite3'),
        }
    }

Templates
*********

To be able to override base templates, it's good idea to put your app
at the first position of ``INSTALLED_APPS`` settings because templates
are searched by order of apps.

If you are using boilerplates (by default it is and by default is used
Bootstrap boilerplate), then you have to override boilerplate template
instead of basic one. More on that topic at :ref:`templates`.

Deployment
**********

You can do it as you are used to to do it. But you are building app so
it's good idea to distribute it as app. For example as Debian (or other)
package. Problem is that not all Python libraries are available as Debian
package which can be solved by deploying app in virtualenv with all needed
packages with tool `dh_virtualenv <http://dh-virtualenv.readthedocs.io/>`_.
