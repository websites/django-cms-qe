# https://gist.github.com/ferrouswheel/7900999

import inspect

from django.utils.encoding import force_str
from django.utils.html import strip_tags


def process_docstring(app, what, name, obj, options, lines):
    # This causes import errors if left outside the function
    from django.db import models

    # Only look at objects that inherit from Django's base model class
    if inspect.isclass(obj) and issubclass(obj, models.Model):
        # Grab the field list from the meta class
        fields = obj._meta.get_fields()

        for field in fields:
            if not hasattr(field, 'attname'):
                continue

            if hasattr(field, 'help_text'):
                # Decode and strip any html out of the field's help text
                help_text = strip_tags(force_str(field.help_text))
                # Add the model field to the end of the docstring as a param
                # using the help text as the description
                lines.append(f':param {field.attname}: {help_text}')
            elif hasattr(field, 'verbose_name'):
                # Decode and capitalize the verbose name, for use if there isn't
                # any help text
                verbose_name = force_str(field.verbose_name).capitalize()
                # Add the model field to the end of the docstring as a param
                # using the verbose name as the description
                lines.append(f':param {field.attname}: {verbose_name}')

            # Add the field's type to the docstring
            if isinstance(field, models.ForeignKey):
                to = field.remote_field.model
                lines.append(f':type {field.attname}: {type(field).__name__} to :class:`~{to.__module__}.{to.__name__}`')
            else:
                lines.append(f':type {field.attname}: {type(field).__name__}')

    # Return the extended docstring
    return lines
