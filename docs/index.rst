Django CMS QE's documentation
=============================

Django CMS Quick & Easy provides all important modules to run new page
without a lot of coding. Aims to do it very easily and securely.

To make it done, it provides a lot of plugins, configurations and gluing
it all together. Django CMS QE has several dependencies which aims to
glue plugins which works together with base safe configuration so not
every app needs to do it again by hand.

Instalation
***********

.. code-block:: bash

    pip install django-cms-qe

Usage
*****

Create your Django app and follow instructions at :doc:`usage`.

Plugins
*******

.. toctree::
   :maxdepth: 2

   plugins
   cms_qe_analytical
   cms_qe_auth
   cms_qe_breadcrumb
   cms_qe_i18n
   cms_qe_menu
   cms_qe_newsletter
   cms_qe_table
   cms_qe_video

API
***

.. toctree::
   :maxdepth: 2

   usage
   cms_qe
   development
