#!/usr/bin/env python3

import sys
from os.path import abspath, dirname, join

sys.path.append(abspath(join(dirname(__file__), '_ext')))

import django

django.setup()


def setup(app):
    from django_sphinx import process_docstring
    app.connect('autodoc-process-docstring', process_docstring)


# -- General configuration ------------------------------------------------

extensions = [
    'djangodocs',
    'sphinx.ext.autodoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.viewcode',
]

templates_path = ['_templates']

source_suffix = '.rst'
master_doc = 'index'

project = 'Django CMS QE'
copyright = '2017, CZ.NIC'
author = 'CZ.NIC'

version = ''
release = version

exclude_patterns = ['_build']

pygments_style = 'sphinx'


# -- Options for HTML output ----------------------------------------------

html_theme = 'alabaster'
# html_theme_options = {}
html_static_path = ['_static']
html_sidebars = {
    '**': [
        'globaltoc.html',
        'relations.html',
        'sourcelink.html',
        'searchbox.html'
    ],
}


# -- Options for HTMLHelp output ------------------------------------------

htmlhelp_basename = 'Django CMS QE Documentation'


# -- Options for intersphinx ----------------------------------------------

intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
    'django': ('https://docs.djangoproject.com/en/1.11/', 'https://docs.djangoproject.com/en/1.11/_objects/'),
}
