"""
Breadcrumb plugin
#################

Plugin for Django CMS QE providing breadcrumb. It allows include breadcrumb
at any place so it can be moved without support of developers.

Usage
*****

Add plugin from section **Generic** called **Breadcrumb**.

API
***

.. autoclass:: cms_qe_breadcrumb.cms_plugins.BreadcrumbPlugin
    :members:

"""
