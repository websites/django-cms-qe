"""
Language plugin
###############

Plugin for Django CMS QE providing language switcher. It allows to add
links for changing language of current page by changing URL.

Usage
*****

Add plugin from section **Generic** called **Language Switcher**.

API
***

.. autoclass:: cms_qe_i18n.cms_plugins.LanguageSwitcherPlugin
    :members:

"""
