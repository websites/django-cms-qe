.PHONY: all cmd prepare-venv prepare-dev venv create-envfile show-python-version test test-smoke test-watch test-selenium test-selenium-clean lint dbshell shell migrate makemesages compilemessages doc run-example run-example-news run-selenium stop-selenium build

# For scripts outside this Makefile use VENV_ACTIVATE, for inside use Python binary directly because it's easier.
VENV_NAME?=venv
VENV_PATH?=$(shell pwd)/${VENV_NAME}
VENV_ACTIVATE=. ${VENV_PATH}/bin/activate
VENV_PYTHON?=python3
PYTHON=${VENV_PATH}/bin/python3

# Use absolute path so it can be used from any directory, for example from docs.
export PYTHONPATH=$(shell pwd)
export DJANGO_SETTINGS_MODULE?=cms_qe.settings.dev

MANAGE=${PYTHON} $(shell pwd)/manage.py
SOURCE_FILES=`ls | grep cms_qe | grep -v egg-info | grep -v cms_qe_test`
test?=${SOURCE_FILES}


all:
	@echo "make prepare-dev"
	@echo "       prepare development environment, use only once"
	@echo "make prepare-venv"
	@echo "       same as prepare-dev, but without apt get update."
	@echo "       Set a folder for a virtual environment. This is optional. Default value is venv."
	@echo "           export VENV_PATH=/tmp/venv"
	@echo "       VENV_PYTHON=/usr/bin/python3.9 make prepare-venv"
	@echo "make test / test-smoke / test-watch"
	@echo "       run tests, or only fast tests, or re-run tests after any change"
	@echo "make test test='--capture=no cms_qe_plugins/tests/test_plugins.py::LinkPluginTests::test_required_attr'"
	@echo "       run only particular test and do not captire stdout."
	@echo "make test-selenium"
	@echo "       run integration tests in browser"
	@echo "make lint"
	@echo "       run pylint and mypy"
	@echo "make shell"
	@echo "       run Django shell"
	@echo "make dbshell"
	@echo "       direct access to the database"
	@echo "make makemigrations"
	@echo "       create migration for new features"
	@echo "make makemessages / compilemessages"
	@echo "       create translation .po files and then compile them into .mo files"
	@echo "make doc"
	@echo "       generate documentation"
	@echo "make run-example"
	@echo "       run example project to test library"
	@echo "make run-example-news"
	@echo "       run example project to test library with Aldryn News&Blog"
	@echo "make build"
	@echo "       create wheel"
	@echo "make clean"
	@echo "       delete virtualenv, byte codes, cache files and generated docs"
	@echo "make clean-db"
	@echo "       delete database and media files"
	@echo "make cmd"
	@echo "       run any Django command"
	@echo "       make cmd=--help cmd"
	@echo "       make cmd=dumpdata cmd"

prepare-venv:
	python3 -m pip install virtualenv
	make venv

# First install all binaries and then prepare Python virtual environment with all dependencies.
prepare-dev:
	@echo "\n  WARNING: For now is fully supported only debian-like systems.\n"
	-sudo apt-get -y update
	-sudo apt-get -y install git
	-sudo apt-get -y install python3 python3-pip memcached
	-sudo apt-get -y install netcat-traditional xvfb # Library for running selenium
	-sudo apt-get -y install libffi-dev # Library for passwords hashing (is using for Argon2)
	-sudo apt-get -y install zlib1g-dev libjpeg-dev libfreetype6-dev python-dev # Install graphic libraries for captcha
	make prepare-venv

# Requirements are in setup.py, so whenever setup.py is changed, re-run installation of dependencies.
venv: create-envfile $(VENV_PATH)/bin/activate show-python-version
$(VENV_PATH)/bin/activate: setup.py
	test -d $(VENV_PATH) || virtualenv -p $(VENV_PYTHON) $(VENV_PATH)
	${PYTHON} -m pip install -U pip
	${PYTHON} -m pip install -e .[dev,test,build]
	${PYTHON} -m pip freeze | grep -v pkg_resources
	echo "Fix 'from imp import reload' in cms/signals/apphook.py for Python versions > 3.4."
	find ${VENV_PATH}/lib -name apphook.py -exec sed -i 's/from imp import reload/from importlib import reload/1' {} \;
	touch $(VENV_PATH)/bin/activate

# Create file with ENV variables possible to use with editors like PyCharm.
create-envfile: DJANGO_SETTINGS_MODULE=example.settings.dev
create-envfile:
	echo "DJANGO_SETTINGS_MODULE=${DJANGO_SETTINGS_MODULE}\nPYTHONPATH=${PYTHONPATH}" > variables.env
show-python-version:
	${PYTHON} --version

test: venv
	@${PYTHON} -m pytest --version
	${PYTHON} -m pytest $(test)

test-lf: venv
	${PYTHON} -m pytest --last-fail

test-smoke: venv
	${PYTHON} -m pytest --skip-slow-tests

test-watch: venv
	${PYTHON} -m pytest_watch --runner "${PYTHON} -m pytest"

test-selenium: DJANGO_SETTINGS_MODULE=example.settings.selenium
test-selenium: PORT=8999
test-selenium: venv
	make test-selenium-clean
	make run-selenium
	WEB_PORT=${PORT} PATH=${PATH}:test_selenium/bin ${PYTHON} -m pytest `find test_selenium -type f -iname test*.py`
	make test-selenium-clean
test-selenium-clean:
	rm -f example/db_selenium.sqlite3
	-fuser -n tcp -k 8999
	sleep 1

lint: venv
	@echo SOURCE_FILES: ${SOURCE_FILES}
	@echo flake8
	@${PYTHON} -m flake8 ${SOURCE_FILES}
	@echo isort --check-only --diff SOURCE_FILES
	@${PYTHON} -m isort --check-only --diff ${SOURCE_FILES}
	@echo pylint --rcfile=pylintrc SOURCE_FILES
	@${PYTHON} -m pylint --rcfile=pylintrc --disable=R ${SOURCE_FILES}
	@${PYTHON} -m mypy --version
	@echo mypy --install-types --non-interactive --ignore-missing-imports SOURCE_FILES
	@${PYTHON} -m mypy --install-types --non-interactive --ignore-missing-imports ${SOURCE_FILES}

shell: venv
	${MANAGE} migrate
	${MANAGE} shell

dbshell: venv
	${MANAGE} migrate
	${MANAGE} dbshell

cmd: venv
	${MANAGE} $(cmd)

makemigrations: venv
	${MANAGE} makemigrations ${SOURCE_FILES}

makemesages: venv
	${MANAGE} makemessages -l en -l cs -i venv -i build
compilemessages: venv
	${MANAGE} compilemessages

doc: venv
	$(VENV_ACTIVATE) && cd docs; make html

run-example: venv
	DJANGO_SETTINGS_MODULE=example.settings.dev PORT=8000 make _run

run-example-news: venv
	${PYTHON} --version
	${PYTHON} -m pip install djangocms-aldryn-newsblog
	DJANGO_SETTINGS_MODULE=example.settings.aldryn_newsblog PORT=8000 make _run

run-selenium: DJANGO_SETTINGS_MODULE=example.settings.selenium
run-selenium: PORT=8999
run-selenium:
	PORT=${PORT} make _run &
	@for i in $(shell seq 1 120); do nc -z 127.0.0.1 ${PORT}; if [ $$? -eq 0 ]; then break; fi; sleep 1; done
	@echo "To stop background process run command: make stop-selenium"
_run: venv
	${MANAGE} migrate
	${MANAGE} create_superuser_if_not_exists
	${MANAGE} runserver 127.0.0.1:${PORT}

stop-selenium:
	ps -f | grep [8]999 | tail -n 1 | awk '{print $$2}' | xargs kill

shell-example: venv
	@echo "For example-news use command: DJANGO_SETTINGS_MODULE=example.settings.aldryn_newsblog make shell"
	${MANAGE} shell

upload:
	python3 setup.py register sdist upload

clean:
	find . -name '*.pyc' -exec rm --force {} +
	rm -rf $(VENV_NAME) *.eggs *.egg-info dist build docs/_build .mypy_cache .cache
clean-db:
	rm -rf db.sqlite3 media

newsletter_sync: venv
	${MANAGE} cms_qe_newsletter_sync
