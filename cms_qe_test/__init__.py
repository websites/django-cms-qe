"""
Utils for testing projects on top of Django CMS QE.
"""

from .cms import render_plugin

__all__ = ['render_plugin']
