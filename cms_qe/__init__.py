"""
Django CMS QE
#############

Django CMS Quick & Easy provides all important modules to run new page
without a lot of coding. Aims to do it very easily and securely.

API
***

Settings
========

.. automodule:: cms_qe.settings
   :members:

Views
=====

.. automodule:: cms_qe.views.errors
   :members:

.. automodule:: cms_qe.views.monitoring
   :members:

.. automodule:: cms_qe.views.security
   :members:

Template tags and filters
=========================

.. automodule:: cms_qe.templatetags.cms_qe_filters
   :members:

Other
=====

.. automodule:: cms_qe.export
   :members:

.. automodule:: cms_qe.staticfiles
   :members:

.. automodule:: cms_qe.utils
   :members:

"""
