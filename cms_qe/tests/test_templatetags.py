from cms_qe.templatetags.cms_qe_filters import add_str, get_sequence_item, matches_pattern, split_by_delimiter


def test_add_str():
    assert add_str(0, 0) == "00"


def test_split_by_delimiter():
    assert split_by_delimiter("1+2", "+") == ['1', '2']


def test_get_sequence_empty():
    assert get_sequence_item([], 0) == ""


def test_get_sequence_index_error():
    assert get_sequence_item("ABC", 3) == ""


def test_get_sequence_str():
    assert get_sequence_item("ABC", 2) == "C"


def test_get_sequence_list():
    assert get_sequence_item(["A", "B", "C"], 2) == "C"


def test_not_matches_pattern():
    assert not matches_pattern("abc", r"\d+")


def test_matches_pattern():
    assert matches_pattern("132", r"\d+")
