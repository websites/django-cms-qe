from unittest.mock import call, patch

from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.db.utils import OperationalError
from django.http import HttpRequest
from django.test import TestCase, override_settings
from django.urls import reverse


class HealthCheckViewTest(TestCase):

    def test(self):
        response = self.client.get(reverse("healthcheck"))
        self.assertContains(response, "OK")

    def test_no_site_domain(self):
        site = Site.objects.first()
        site.domain = ""
        site.save()
        with self.assertRaisesMessage(RuntimeError, 'db'):
            self.client.get(reverse("healthcheck"))

    @patch("cms_qe.views.maintenance.Site.objects.first")
    def test_no_db(self, mock_site_first):
        message = "connection to server failed"
        mock_site_first.side_effect = OperationalError(message)
        with self.assertRaisesMessage(OperationalError, message):
            self.client.get(reverse("healthcheck"))

    @patch("cms_qe.views.maintenance.cache")
    def test_not_working_cache(self, mock_cache):
        mock_cache.get.return_value = ""
        with self.assertRaisesMessage(RuntimeError, 'cache'):
            self.client.get(reverse("healthcheck"))

    @override_settings(HEALTHCHECK_FUNCTIONS=['path.to.fnc'])
    def test_fnc_invalid_path(self):
        with self.assertRaisesMessage(ModuleNotFoundError, "No module named 'path'"):
            self.client.get(reverse("healthcheck"))

    @override_settings(HEALTHCHECK_FUNCTIONS=['cms_qe.tests.test_views_maintenance.healthcheck_raise'])
    def test_fnc_raise(self):
        with self.assertRaisesMessage(RuntimeError, "TEST"):
            self.client.get(reverse("healthcheck"))

    @override_settings(HEALTHCHECK_FUNCTIONS=['cms_qe.tests.test_views_maintenance.healthcheck'])
    def test_fnc(self):
        response = self.client.get(reverse("healthcheck"))
        self.assertEqual(response.wsgi_request.healthcheck, "OK")


class ReloadSiteViewTest(TestCase):

    def _login_admin(self):
        admin = get_user_model().objects.create(username="admin", is_superuser=True)
        self.client.force_login(admin)

    def test_no_superuser(self):
        response = self.client.get(reverse("reload-site"))
        self.assertContains(response, "ERROR: User is not is_superuser.")

    def test_no_settings(self):
        self._login_admin()
        response = self.client.get(reverse("reload-site"))
        self.assertContains(response, "SKIP: No settings.RELOAD_SITE.")

    @override_settings(RELOAD_SITE=['ls', '-l'])
    @patch("cms_qe.signals.subprocess.run")
    def test_subprocess_run(self, mock_run):
        mock_run.return_value = "total 0"
        self._login_admin()
        response = self.client.get(reverse("reload-site"))
        self.assertContains(response, "total 0")
        self.assertEqual(mock_run.mock_calls, [
            call(['ls', '-l'], check=False)
        ])


def healthcheck_raise(request: HttpRequest, *args, **kwargs) -> None:
    """Checking that the system is functional."""
    raise RuntimeError('TEST')


def healthcheck(request: HttpRequest, *args, **kwargs) -> None:
    """Checking that the system is functional."""
    request.healthcheck = "OK"
