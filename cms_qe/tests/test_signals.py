from multiprocessing import Process
from unittest.mock import call, patch

from django.test import override_settings
from testfixtures import LogCapture

from cms_qe.signals import reload_site, run_process, run_subprocess


@patch("cms_qe.signals.subprocess.run")
def test_run_subprocess(mock_run):
    mock_run.return_value = "total 0"
    with LogCapture("cms_qe.signals") as log:
        response = run_subprocess(['ls', '-l'])
    assert response == "total 0"
    assert mock_run.mock_calls == [call(['ls', '-l'], check=False)]
    log.check(
        ('cms_qe.signals', 'INFO', "subprocess.run(['ls', '-l'])"),
    )


@patch("cms_qe.signals.subprocess.run")
def test_run_subprocess_with_timeout(mock_run):
    mock_run.return_value = "total 0"
    with LogCapture("cms_qe.signals") as log:
        response = run_subprocess(['ls', '-l'], 1)
    assert response == "total 0"
    assert mock_run.mock_calls == [call(['ls', '-l'], check=False)]
    log.check(
        ('cms_qe.signals', 'INFO', "subprocess.run(['ls', '-l'])"),
    )


@patch("cms_qe.signals.subprocess.run")
def test_run_subprocess_exception(mock_run):
    mock_run.side_effect = Exception("Error!")
    with LogCapture("cms_qe.signals") as log:
        response = run_subprocess(['ls', '-l'])
    assert response is None
    assert mock_run.mock_calls == [call(['ls', '-l'], check=False)]
    log.check(
        ('cms_qe.signals', 'INFO', "subprocess.run(['ls', '-l'])"),
        ('cms_qe.signals', 'ERROR', "Error!"),
    )


@patch("cms_qe.signals.Process.start")
def test_run_process(mock_start):
    process = run_process(['echo', '"Hello world!"'])
    assert type(process) is Process
    assert mock_start.mock_calls == [call()]


@patch("cms_qe.signals.Process.start")
def test_run_process_with_timeout(mock_start):
    process = run_process(['echo', '"Hello world with timeout."'], 1)
    assert type(process) is Process
    assert mock_start.mock_calls == [call()]


@patch("cms_qe.signals.Process.start")
def test_reload_site_no_settings(mock_start):
    reload_site(None)
    assert mock_start.mock_calls == []


@override_settings(RELOAD_SITE=['echo', '"The site!"'])
@patch("cms_qe.signals.Process.start")
def test_reload_site(mock_start):
    reload_site(None)
    assert mock_start.mock_calls == [call()]
