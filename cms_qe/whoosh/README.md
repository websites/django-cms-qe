How to enable search engine
===========================

Guide how to enable search engine.

setup.py:

```python
setup(
    install_requires=[
        ...
        'django-haystack~=3.2',
        'whoosh~=2.7',
        'djangocms-aldryn-search',
    ]
)
```

settings.py:

```python
import os
from pathlib import Path

INSTALLED_APPS = [
    ...
    # Serach engine
    'haystack',
    'standard_form',
    'spurl',
    'aldryn_search',
]

site_resolver = Path(__file__).resolve()

PROJECT_DIR = site_resolver.parent.parent.parent.parent

HAYSTACK_ROUTERS = ['aldryn_search.router.LanguageRouter']
HAYSTACK_ENGINE = 'cms_qe.whoosh.backend.AnalyzerWhooshEngine'
_HAYSTACK_PATH = os.path.normpath(os.path.join(PROJECT_DIR, 'whoosh_index'))
HAYSTACK_CONNECTIONS = {
    'default': {'ENGINE': HAYSTACK_ENGINE, 'PATH': os.path.join(_HAYSTACK_PATH, 'default')},
    'en': {'ENGINE': HAYSTACK_ENGINE, 'PATH': os.path.join(_HAYSTACK_PATH, 'en')},
}
HAYSTACK_CUSTOM_HIGHLIGHTER = "cms_qe.haystack.highlighting.HaystackHighlighter"
```

cms_qe/views/search_result.py

```python
from aldryn_search.views import AldrynSearchView
from cms_qe.haystack.forms import HaystackSearchForm


class SiteSearchView(AldrynSearchView):
    """Site Search View."""

    form_class = HaystackSearchForm
```

urls.py:

```python
from cms_qe.views.search_result import SiteSearchView

urlpatterns = [
    ...
    path('search-result/', SiteSearchView.as_view(), name='site-aldryn-search'),
]
```


Test the search page at http://localhost:8000/search-result/.
