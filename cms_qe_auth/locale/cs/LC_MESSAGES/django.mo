��    5      �  G   l      �  �   �  �   Q  �   �  �   w  �   "  l   �          1     M     R     e     u     �     �     �     �  '   �  l   		     v	  {   �	     
     
     
  
   %
     0
     6
  
   C
     N
     ^
     t
     {
     �
     �
  P   �
  �   	  :   �     �     �     �     �            G   )  >   q     �     �  u   �  4   X  �   �  =        W  (   r  ]  �  �   �     �  �     �   �  �   )  �   �     C  !   [     }     �     �     �     �     �     �     �  0     n   4     �  �   �     t     �     �     �     �     �     �     �       	   )     3     F     S  I   p  ]   �  <        U     a     |     �     �     �  N   �  F     *   S     ~  g   �  8   �  �   2  ?   �     �  '                  ,       	   )           0      1      $          -   +   !          3   
       (   &      %                        5                 *                       #                          '             2          .          4         "   /       
                Hello %(username)s,
                please confirm your email address, to activate your account. If you
                received this email by mistake, simply ignore it.
             
            You're receiving this email because you requested a password reset for your user account at %(site_name)s.
         
        You are already logged in as <strong>%(username)s</strong>.
        Do you want to <a href="%(logout_url)s?next=%(register_url)s">register anyway</a>?
     
        You are already logged in as <strong>%(username)s</strong>.
        Do you want to log in as <a href="%(logout_url)s?next=%(login_url)s">different user</a>?
     
Hello %(username)s,
please confirm your email address, to activate your account. If you received this email by mistake, simply ignore it.
 
You're receiving this email because you requested a password reset for your user account at %(site_name)s.
 Activate your account Activation link is invalid! Auth Change my password Change password Confirm email address Confirm password: Email confirmation Email confirmation address Email does not exists Enter a valid email or don't enter any. Forgotten your password? Enter your email address below, and we'll email instructions for setting a new one. Go to homepage If you don't receive an email, please make sure you've entered the address you registered with, and check your spam folder. Log in Log in again Log in instead? Logged out Login Login button Login form Login is locked Login to Your Account Logout Lost password? New password: Password reset on %(site_name)s Please enter your new password twice so we can verify you typed it in correctly. Please enter your old password, for security's sake, and then enter your new password twice so we can verify you typed it in correctly. Please go to the following page and choose a new password: Register Register a new account Register form Reset my password Reset password Reset pasword Thank you for your email confirmation. You was logged in automatically. Thanks for spending some quality time with the Web site today. Thanks for using our site! The %(site_name)s team The password reset link was invalid, possibly because it has already been used.  Please request a new password reset. Too many attempts to log in. Please try again later. We've emailed you instructions for setting your password, if an account exists with the email you entered. You should receive them shortly. Your password has been set.  You may go ahead and log in now. Your password was changed. Your username, in case you've forgotten: Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 
Dobrý den %(username)s,
prosíme, ověřte vaši e-mailovou adresu pro aktivaci vašeho účtu. Pokud jste dostal tento e-mail omylem, ignorujte jej. 
Tento e-mail vám byl zaslán na základě vyžádání obnovy hesla vašeho uživatelskému účtu na systému %(site_name)s. 
Jste přihlášen jako <strong>%(username)s</strong>.
Chcete se <a href="%(logout_url)s?next=%(register_url)s>i tak registrovat</a>? 
Jste přihlášen jako <strong>%(username)s</strong>.
Chcete se přihlásit jako <a href="%(logout_url)s?next=%(login_url)s>jiný uživatel</a>? 
Dobrý den %(username)s,
prosíme, ověřte vaši e-mailovou adresu pro aktivaci vašeho účtu. Pokud jste dostal tento e-mail omylem, ignorujte jej.
 
Tento e-mail vám byl zaslán na základě vyžádání obnovy hesla vašeho uživatelskému účtu na systému %(site_name)s.
 Aktivace vašeho účtu Aktivační odkaz není validní! Auth Změnit heslo Změnit heslo Ověřit e-mailovou adresu Potvrdit heslo: Ověření e-mailu Konfirmační e-mail E-mail neexistuje Zadejte platný e-mail nebo nedávejte žádný. Zapomněli jste heslo? Zadejte níže e-mailovou adresu a systém vám odešle instrukce k nastavení nového. Přejít na domovskou stránku Pokud e-mail neobdržíte, ujistěte se, že zadaná e-mailová adresa je stejná jako ta registrovaná u vašeho účtu a zkontrolujte složku nevyžádané pošty, tzv. spamu. Přihlásit Přihlaste se znovu Raději se přihlásit? Odhlášeno Přihlásit Formulář na přihlášení Formulář na přihlášení Přihlášení zablokováno Přihlásit se Odhlásit Zapomenuté heslo? Nové heslo: Reset hesla na %(site_name)s Zadejte dvakrát nové heslo. Tak ověříme, že bylo zadáno správně. Zadejte svoje současné heslo a poté dvakrát heslo nové. Omezíme tak možnost překlepu. Přejděte na následující stránku a zadejte nové heslo: Registrovat Zaregistrovat nový účet Formulář na registraci Obnovit heslo Reset hesla Obnovit heslo Děkujeme za vaše e-mailové ověření. Byli jste automaticky přihlášeni. Děkujeme za dnešní kvalitní strávení času s našimi stránkami. Děkujeme za používaní naší stránky! Tým %(site_name)s Odkaz pro obnovení hesla byl neplatný, možná již byl použit. Požádejte o obnovení hesla znovu. Příliš mnoho pokusů. Prosíme, zkuste to za chvíli. Návod na nastavení hesla byl odeslán na zadanou e-mailovou adresu, pokud účet s takovou adresou existuje. Měl by za okamžik dorazit. Vaše heslo bylo nastaveno. Můžete se jít nyní přihlásit. Vyše heslo bylo změněno. Pro jistotu vaše uživatelské jméno: 