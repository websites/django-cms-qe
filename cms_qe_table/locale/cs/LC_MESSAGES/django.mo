��          |      �             !  s   1  #   �     �     �     �     �               ?     M  �  `     �  v   �  .   s     �     �     �     �          
     (     =              
                          	                   Columns to show Drag & drop certain columns to "Selected columns" list to show them on the page. You can also change columns order. How many items per page when paging Non existing table {}. Selected columns Show paging Some column does not exist! Table Table %(table)s does not exist! Table to show Unselected columns Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-08-31 13:44+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Sloupce k zobrazení Přetáhněte sloupce do seznamu "Vybrané sloupce" pro zobrazení na stránce.Můžete také změnit jejich pořadí. Počet položek na stránku pro stránkování Neexistující tablka {}. Vybrané sloupce Zobrazit stránkování Nějaký sloupec neexistuje! Tabulka Tabulka %(table)s neexistuje! Tabulka k zobrazení Nevybrané sloupce 