import pytest

# Import all fixtures from all modules.
from cms_qe.fixtures import *
from cms_qe_auth.fixtures import *
from cms_qe_newsletter.fixtures import *
from cms_qe_table.fixtures import *
from cms_qe_video.fixtures import *


def pytest_addoption(parser):
    parser.addoption('--skip-slow-tests', action='store_true', help='skip slow tests')


def pytest_runtest_setup(item):
    if 'slow' in item.keywords and item.config.getoption('--skip-slow-tests'):
        pytest.skip('Skipping slow test (used param --skip-slow-tests)')


@pytest.fixture(autouse=True)
def enable_db_access_for_all_tests(db):
    pass
