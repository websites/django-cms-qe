��          \      �       �   �   �   	   �  
   �      �     �     �       �    �   �     s  
   �  %   �     �     �     �                                       
            Specify at which level the navigation should stop.

            Keep in mind that Bootstrap by default does not support dropdown submenu.
            Any deep dropdown menu is not very well accessible from mobile devices.
         End level Horizontal ID of page in advanced settings. Menu Submenu for inactive items Vertical Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-08-31 13:44+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 
Specifikujte do jakého zanoření se má navigace zobrazit.

Mějte na paměti, že Bootstrap defaultně nepodporuje podmenu.
Žádné zanořené menu není velmi dobře dostupné pro mobilní zařízení. Maximální zanoření Vodorovně ID stránky v pokročilém nastavení Menu Podmenu pro neaktivní položky Svisle 