from cms.api import create_page
from cms.test_utils.testcases import CMSTestCase
from constance.test import override_config
from django.test import override_settings


@override_settings(ROOT_URLCONF='cms_qe.urls')
@override_config(GOOGLE_TAG_MANAGER_ID='G-XXXXXX')
class GoogleTagManagerTestCase(CMSTestCase):
    """
    Tests for the ``google_tag_manager.html`` template.
    """

    def setUp(self):
        self.page = create_page('test page', 'cms_qe/home.html', 'en', published=True)

    def test_tag(self):
        response = self.client.get(self.page.get_absolute_url('en'))
        self.assertContains(response, """
            <head>
                <title>test page</title>
                <link rel="stylesheet" href="/static/css/example.css">
                <!-- Google tag (gtag.js) -->
                <script async src="https://www.googletagmanager.com/gtag/js?id=G-XXXXXX"></script>
                <script>
                    window.dataLayer = window.dataLayer || [];
                    function gtag() {dataLayer.push(arguments)}
                    gtag('js', new Date());
                    gtag('config', 'G-XXXXXX');
                </script>
            </head>""", html=True)

    def test_no_tag(self):
        with override_config(GOOGLE_TAG_MANAGER_ID=''):
            response = self.client.get(self.page.get_absolute_url('en'))
        self.assertContains(response, """
            <head>
                <title>test page</title>
                <link rel="stylesheet" href="/static/css/example.css">
            </head>""", html=True)

    def test_tag_script(self):
        with override_config(GOOGLE_TAG_MANAGER_SCRIPT="gtag('config', 'DC-ZZZZZZ');"):
            response = self.client.get(self.page.get_absolute_url('en'))
        self.assertContains(response, """
            <head>
                <title>test page</title>
                <link rel="stylesheet" href="/static/css/example.css">
                <!-- Google tag (gtag.js) -->
                <script async src="https://www.googletagmanager.com/gtag/js?id=G-XXXXXX"></script>
                <script>
                    window.dataLayer = window.dataLayer || [];
                    function gtag() {dataLayer.push(arguments)}
                    gtag('js', new Date());
                    gtag('config', 'G-XXXXXX');
                    gtag('config', 'DC-ZZZZZZ');
                </script>
            </head>""", html=True)
