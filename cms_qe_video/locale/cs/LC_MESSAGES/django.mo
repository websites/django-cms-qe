��    &      L  5   |      P     Q     c  
   u     �     �     �     �     �  
   �     �     �  !   �          	                    *     1     A     H     V     ]     i  	   y     �     �     �     �  W   �     #     9     F     f  '   �     �  /   �  �  �     �     �     �     �     �     �                    !     >  %   F     l  
   q     |     �     �     �     �     �     �     �     �     �     	     	     	     $	  #   >	  c   b	     �	     �	  (   �	  %   
  2   B
     u
  3   ~
                             $                    &                               #   !                       
   "            %                                                     	    <file is missing> Advanced settings Attributes Autoplay Captions Chapters Description Descriptions Embed link Examples: "en" or "de" etc. Height Incorrect file type: {extension}. Kind Label Loop Mute Other attributes Others Player settings Poster Show controls Source Source file Source language Subtitles Title Track URL does not belong to Vimeo URL does not belong to YouTube Use this field to embed videos from external services such as YouTube, Vimeo or others. Video hosting service Video player Video player - hosting services Video player - source file Vimeo does not support hiding controls. Width Your browser doesn't support this video format. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-08-31 13:44+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Angelo Dini <finalangeljp@hotmail.com>, 2016
Language-Team: Czech (https://www.transifex.com/divio/teams/58664/cs/)
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 <file is missing> Pokročilé nastavení Atributy Automatické přehrávání Popisek(Captions) Kapitoly Popis Popis Vložit odkaz Příklad: "en" or "de" etc. Výška Nesprávný typ souboru: {extension}. Druh Označení Opakovat Ztlumit Další atributy Ostatní Nastavení zdrojového souboru Plakát Zobrazit ovládací prvky Zdroj Zdrojový soubor Zdrojový jazyk Titulky Název Track Adresa nepatří do Vimeo Adresa nepatří do služby YouTube Toto pole použijte pro vkládání videí z externích služeb, jako je YouTube, Vimeo nebo jiné. Video hostingové služby Video přehrávač Video přehrávač - hostingové služby Video přehrávač - zdrojový soubor Vimeo nepodporuje skrývání ovládacích prvků. Šířka Váš prohlížeč tento formát videa nepodporuje. 