from cms.api import add_plugin
from cms.models import Placeholder
from cms.plugin_rendering import ContentRenderer
from cms.toolbar.toolbar import CMSToolbar
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.test.client import RequestFactory

from cms_qe_plugins.cms_plugins import IframeTagPlugin, LinkTagPlugin, PublishedOrDraftContentPlugin, ScriptTagPlugin


class LinkTagPluginTests(TestCase):

    def test_required_attr(self):
        placeholder = Placeholder.objects.create(slot='test')
        plugin = add_plugin(placeholder, LinkTagPlugin, 'en', href='/path/to/data.js')
        renderer = ContentRenderer(request=RequestFactory())
        html = renderer.render_plugin(plugin, {})
        self.assertHTMLEqual(html, """<link href="/path/to/data.js" rel="stylesheet">""")

    def test_all_attrs(self):
        placeholder = Placeholder.objects.create(slot='test')
        plugin = add_plugin(
            placeholder,
            LinkTagPlugin,
            'en',
            href='/path/to/data.js',
            rel='preload',
            cross_origin='anonymous',
            hreflang='cs_CZ',
            media='print',
            referrer_policy='origin',
            sizes='42x84',
            title='main',
            type='text/css',
            attributes={"class": "lnk", "id": 42}
        )
        renderer = ContentRenderer(request=RequestFactory())
        html = renderer.render_plugin(plugin, {})
        self.assertHTMLEqual(html, """
            <link href="/path/to/data.js" rel="preload" crossorigin="anonymous"
                hreflang="cs_CZ" media="print" referrerpolicy="origin" sizes="42x84" title="main" type="text/css"
                class="lnk" id="42">""")


class ScriptTagPluginTests(TestCase):

    def test_required_attr(self):
        placeholder = Placeholder.objects.create(slot='test')
        plugin = add_plugin(placeholder, ScriptTagPlugin, 'en', src='/path/to/data.js')
        renderer = ContentRenderer(request=RequestFactory())
        html = renderer.render_plugin(plugin, {})
        self.assertHTMLEqual(html, """<script src="/path/to/data.js"></script>""")

    def test_all_attrs(self):
        placeholder = Placeholder.objects.create(slot='test')
        plugin = add_plugin(
            placeholder,
            ScriptTagPlugin,
            'en',
            src='/path/to/data.js',
            asyncf=True,
            cross_origin='anonymous',
            defer=True,
            integrity='ef098763',
            nomodule=True,
            referrer_policy='no-referrer',
            type='text/js',
            attributes={"class": "lnk", "id": 42}
        )
        renderer = ContentRenderer(request=RequestFactory())
        html = renderer.render_plugin(plugin, {})
        self.assertHTMLEqual(
            html, """<script src="/path/to/data.js" async crossorigin="anonymous" defer integrity="ef098763"
                referrerpolicy="no-referrer" nomodule type="text/js" class="lnk" id="42"></script>""")


class IframeTagPluginTests(TestCase):

    def test_required_attr(self):
        placeholder = Placeholder.objects.create(slot='test')
        plugin = add_plugin(placeholder, IframeTagPlugin, 'en', src='/path/to/page.html')
        renderer = ContentRenderer(request=RequestFactory())
        html = renderer.render_plugin(plugin, {})
        self.assertHTMLEqual(html, """<iframe src="/path/to/page.html"></iframe>""")

    def test_all_attrs(self):
        placeholder = Placeholder.objects.create(slot='test')
        plugin = add_plugin(
            placeholder,
            IframeTagPlugin,
            'en',
            src='/path/to/page.html',
            allow='(self)',
            allow_full_screen=True,
            allow_payment_request=True,
            height=42,
            width=84,
            loading='lazy',
            name='rimmer',
            referrer_policy='origin',
            sandbox='allow-forms',
            src_doc='const name = "The name"',
            attributes={"class": "lnk", "id": 42}
        )
        renderer = ContentRenderer(request=RequestFactory())
        html = renderer.render_plugin(plugin, {})
        self.assertHTMLEqual(
            html, """<iframe src="/path/to/page.html" allow allowfullscreen="1" allowpaymentrequest="1" height="42"
                width="84" loading="lazy" name="rimmer" referrerpolicy="origin" sandbox="allow-forms"
                srcdoc="const name = &quot;The name&quot;" class="lnk" id="42"></iframe>""")


class PublishedOrDraftContentPluginTest(TestCase):

    def setUp(self):
        self.placeholder = Placeholder.objects.create(slot='test')

    def test_published(self):
        plugin = add_plugin(self.placeholder, PublishedOrDraftContentPlugin, 'en')
        link = add_plugin(self.placeholder, LinkTagPlugin, 'en', href='/data.js')
        plugin.child_plugin_instances = [link]
        request = RequestFactory().get("/page")
        request.current_page = None
        renderer = ContentRenderer(request=request)
        html = renderer.render_plugin(plugin, {"request": request})
        self.assertHTMLEqual(
            html, """
            <span class="content-published published-or-draft">
                <link href="/data.js" rel="stylesheet">
            </span>""")

    def test_draft(self):
        plugin = add_plugin(self.placeholder, PublishedOrDraftContentPlugin, 'en')
        link = add_plugin(self.placeholder, LinkTagPlugin, 'en', href='/data.js')
        plugin.child_plugin_instances = [link]
        request = RequestFactory().get("/page")
        request.current_page = None
        request.user = get_user_model().objects.create(username="user", is_superuser=True, is_staff=True)
        request.session = {"cms_edit": True}
        request.toolbar = CMSToolbar(request)
        renderer = ContentRenderer(request=request)
        html = renderer.render_plugin(plugin, {"user": request.user, "request": request})
        self.assertHTMLEqual(
            html, """
            <span class="content-draft published-or-draft">
                <template class="cms-plugin cms-plugin-start cms-plugin-2"></template>
                <link href="/data.js" rel="stylesheet">
                <template class="cms-plugin cms-plugin-end cms-plugin-2"></template>
            </span>""")
